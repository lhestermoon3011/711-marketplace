import React, { useEffect } from 'react';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Container, Card, Row, Col, Image, Button, Table } from 'react-bootstrap';

const Wallet = () => {
  const { balance } = useStoreState(state => state);
  const { fetchBalance } = useStoreActions(action => action);

  useEffect(() => {
    fetchBalance('a@a.com');
  }, []); //eslint-disable-line

  return (
    <Container style={{ marginTop: '7rem' }}>
      <div>
        <h1>My Wallet</h1>
        {balance ?
        <div className="float-right">
          <p style={{display: 'inline'}} className="display-4">{balance.balance}</p>
          <h4 style={{display: 'inline'}} className="text-secondary">SC</h4>
        </div>
        : 
          <p>Loading balance, please wait...</p>
        }
      </div>
      <div className="clearfix"/>
      <hr/>
      <h3 className="text-right">Remaining Balance (Stablecoin)</h3>
    </Container>
  )
}

export default Wallet;
