import React, { useState } from 'react';
import { Container, Tab, Nav, Row, Col } from 'react-bootstrap';
import CheckoutComponents from '../../components/CheckoutComponents';


const Checkout = () => {
  const [ step, setStep ] = useState(1);
  const active = {background: '#101010', color: '#fff'};
  const handleSteps = (step) => {
    console.log(step);
    switch(step) {
      case 1:
        return (
          <CheckoutComponents setStep={setStep} eventKey={1}/>
          )
      case 2: 
        return (
          <CheckoutComponents setStep={setStep} eventKey={2}/>
          )
      case 3: 
        return (
          <CheckoutComponents setStep={setStep} eventKey={3}/>     
          )
      case 4: 
        return (
          <CheckoutComponents setStep={setStep} eventKey={4}/>          
          )
      default:
        return null;
    }
  }

  return (
    <Container style={{ marginTop: '7rem' }}>
      <h1 className="mb-3">Checkout</h1>
      <Tab.Container id="left-tabs-example" defaultActiveKey={1}>
        <Row>
          <Col lg={3}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link style={step === 1 ? active : null}>Costumer Information</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link  
                  style={step === 2 ? active : null}>Delivery Details</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link  
                  style={step === 3 ? active : null}>Payment Method</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link 
                  style={step === 4 ? active : null}>Order Review</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col lg={9}>
            {handleSteps(step)}
          </Col>
        </Row>
      </Tab.Container>
    </Container>
  )
}

export default Checkout;