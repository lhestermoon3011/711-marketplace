import React, { useState } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { Container, Card, Row, Col, Image, Button, Form, InputGroup, FormControl } from 'react-bootstrap';
import { categoryIdToCategory, itemIdToItem } from '../../utils/IdToData';
import StarRating from '../../components/StarRating';
import CartList from '../../components/CartList';

const Cart = (props) => {
  const { history } = props;
  const itemId = parseInt(props.match.params.id);
  const itemData = itemIdToItem(itemId);
  const catData = categoryIdToCategory(itemData.categoryId);

  const { cartAmountTotals } = useStoreState(state => state);
  const { saveItemToCart } = useStoreActions(action => action);

  const [ quantity, setQuantity ] = useState(1);
  const [ itemRemoved, setItemRemoved ] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const totalAmount = itemData.item_price * quantity;
  const cartData = { itemId, quantity, totalAmount };
    saveItemToCart(cartData);
    setItemRemoved(true);
  };

  if(cartAmountTotals !== []) { 
    console.log(cartAmountTotals.reduce((a, b) => a + b));
  }
  return (
    <Container style={{ marginTop: '7rem' }}>
      <div>
        <Button className="float-right mb-3" onClick={() => history.push('/checkout')}>Proceed to Checkout <i className="fa fa-chevron-right fa-fw"/></Button>
        <h1>Add to Cart</h1>
      </div>
      <div className="clearfix"/>
      {!itemRemoved ? 
      <Card className="mb-3">
        <Card.Body>
          <Row>
            <Col lg={3}>
              <Image src={itemData.item_photos[0]} fluid />
            </Col>
            <Col lg={6}>
              <p className="mb-1"><StarRating rating={itemData.item_rating}/></p>
              <h3>{itemData.item_name}</h3>
              <h5 className="text-secondary">{`P${itemData.item_price}`} &bull; {catData.name}</h5>
              <Button className="mt-4">More Details </Button>
            </Col>
            <Col lg={3}>
              <Form onSubmit={(e) => handleSubmit(e)}>
                <Form.Group>
                  <Form.Label>Quantity</Form.Label>
                  <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                      <Button onClick={() => setQuantity(quantity+1)} variant="outline-primary"><i className="fa fa-plus fa-fw"/></Button>
                    </InputGroup.Prepend>
                    <FormControl value={quantity} onChange={(e) => setQuantity(e.target.value)} aria-describedby="basic-addon1" />
                    <InputGroup.Prepend>
                      <Button onClick={() => setQuantity(quantity-1)} variant="outline-primary"><i className="fa fa-minus fa-fw"/></Button>
                    </InputGroup.Prepend>
                  </InputGroup>
                </Form.Group>
                <Button type="submit" className="btn-block">Add to Cart</Button>
              </Form>
            </Col>
          </Row>
        </Card.Body>
      </Card>
      : 
      <p className="text-center">
        <Button onClick={() => history.goBack()}>Return to Shop <i className="fa fa-store fa-fw"/></Button>
      </p>
      }
      <CartList/>
    </Container>
  )
}

export default Cart;