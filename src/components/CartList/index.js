import React from 'react';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Card, Button, Image, Row, Col } from 'react-bootstrap';
import { itemIdToItem, categoryIdToCategory } from '../../utils/IdToData';
import StarRating from '../../components/StarRating';

const CartList = () => {
  const { removeItemToCart } = useStoreActions(action => action);
  const { cart } = useStoreState(state => state);


  return (
    <div>
      <h1>My Cart ({cart.length})</h1>
      {cart ? 
        cart.map((cartData, i) => {
          const itemData = itemIdToItem(cartData.itemId);
          const catData = categoryIdToCategory(itemData.categoryId);

          return (
            <Card key={i} className="mb-2">
              <Card.Body>
                <Row>
                  <Col lg={3}>
                    <Image src={itemData.item_photos[0]} fluid />
                  </Col>
                  <Col lg={6}>
                    <p className="mb-1"><StarRating rating={itemData.item_rating}/></p>
                    <h3>{itemData.item_name}</h3>
                    <h5 className="text-secondary">{`P${itemData.item_price}`} &bull; {catData.name}</h5>
                    <Button className="mt-4">More Details </Button>
                  </Col>
                  <Col lg={2}>
                    <p>Quantity</p>
                    <h3>{cartData.quantity}</h3>
                    <p>Total Amount</p>
                    <h3>P{cartData.totalAmount}</h3>
                  </Col>
                  <Col lg={1}>
                    <Button onClick={() => removeItemToCart(itemData.id)} className="float-right btn-sm" variant="light"><i className="fa fa-times fa-2x fa-fw"/></Button>
                    <div className="clearfix"/>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          )
        })
      :
      <p className="text-center">Loading, please wait...</p>
      }
    </div>
  )
}


export default CartList;
