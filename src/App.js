import React from 'react';
import { StoreProvider, createStore } from 'easy-peasy';
import './App.css';
import { HashRouter } from 'react-router-dom';
import Header from './components/Header';
import { routes } from './routes';
import model from '../src/models';

const store = createStore(model);

const App = () => {
  return (
    <StoreProvider store={store}>
      <HashRouter>
        <Header/>
        {routes}
      </HashRouter>
    </StoreProvider>
  );
}

export default App;
