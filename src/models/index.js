import { action, thunk } from 'easy-peasy';
import axios from 'axios';

export default {
  cart: [],
  userData: null,
  balance: 0,
  cartAmountTotals: [0],
  loading: false,
  fetchUser: thunk(async (actions) => {
    axios
    .post('http://192.168.152.210:8085/api/auth/login', { email: 'a@a.com' })
    .then((res) => {
      actions.setUser(res.data);
    })
    .catch((err) => {
      console.log(err);
    })
  }),
  fetchBalance: thunk(async (actions, email) => {
    axios
    .get(`http://192.168.152.210:8085/api/account/balance?email=${email}`)
    .then((res) => {
      console.log(res.data.result);
      actions.setBalance(res.data.result);
    })
    .catch((err) => {
      console.log(err);
    })
  }),
  transferBalance: thunk(async (actions, transferData) => {
    axios
    .post(`http://192.168.152.210:8085/api/account/transfer`, transferData)
    .then((res) => {
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
  }),
  saveItemToCart: action((state, cartData) => {
    state.cart = [cartData, ...state.cart];
    state.cartAmountTotals = [cartData.totalAmount, ...state.cartAmountTotals];
  }),
  removeItemToCart: action((state, itemId) => {
    state.cart = state.cart.filter(cart => cart.itemId !== itemId);
  }),
  setUser: action((state, user) => {
    state.userData = user;
  }), 
  setBalance: action((state, balance) => {
    state.balance = balance;
  }), 
  loadingBegin: action((state) => {
  state.loading = true;
  }),
  loadingEnd: action((state) => {
    state.loading = false;
  }),
}